Server API (IPv4 Public IP => 18.220.43.133)
======
Authentication is not needed
-----
#### Get All Users
**Route**

    GET /api/all-users
    
**Response**

    [
        {
            "id": 1,
            "name": "Ettie Spinka",
            "email": "jakubowski.whitney@example.com",
            "remember_token": "Iwny1X4TYS",
            "created_at": "2017-08-01 01:49:26",
            "updated_at": "2017-08-01 01:49:26"
        }
    ]

#### Get All Rooms
**Route**

    GET /api/all-rooms
    
**Response**

    200 OK
    [
        {
            "id": 1,
            "room_number": 101,
            "capacity": 1,
            "created_at": "2017-08-01 01:49:26",
            "updated_at": "2017-08-01 01:49:26"
        }
    ]

#### Get All Bookings
**Route**

    GET /api/all-bookins
    
**Response**

    [
        {
            "id": 1,
            "user_id": 11,
            "room_id": 2,
            "started_at": "2017-07-02 01:00:00",
            "ended_at": "2017-07-02 01:30:00",
            "canceled": 0,
            "created_at": "2017-08-01 01:49:53",
            "updated_at": "2017-08-01 01:49:53"
        }
    ]

### Get All Invitations
**Route**

    GET /api/all-invitations
    
**Response**

    [
        {
            "id": 1,
            "inviter_id": 11,
            "invitee_id": 3,
            "booking_id": 1,
            "status": 0,
            "created_at": "2017-08-01 01:50:30",
            "updated_at": "2017-08-01 01:50:30"
        }
    ]
    
#### Register
**Route**

    POST /api/register
**Body**

    {
    	"name" : "test",
    	"email": "test@gmail.com",
    	"password": "testqwd",
    	"password_confirmation": "testqwd"
    }
**Response**

    {"status":"success","message":"Registered succefully"}
#### Login
**Route**

    POST /api/login
**Body**

    {
    	"email": "test@gmail.com",
    	"password": "testqwd",
    }
**Response**



Authentication needed
-----
#### Logout 
**Route**

    GET /api/Logout
**Response**

    {"status":"success","message":"Logged out succefully"}

#### Unregister
**Route**

    DELETE /api/unregister
**Response**

    {"status":"success","message":"Registered succefully"}

#### Book a meeting room
**Route**

    POST /api/booking/book
**Body**

    {
      "room_number" : "102",
      "started_at" : "2017-08-02 13:00",
      "ended_at" : "2017-08-02 14:30",
    }
**Response**

    {"status":"success","message":"This booking created succefully."}
    or
    {"status":"error","message":"Booking time is overlapped"}
    
#### Cancel a meeting room
**Route**

    POST /api/booking/cancel
**Body**

    {
	    "booking_id": 1,
    }
**Response**

    {"status":"success","message":"This booking is canceld succefully."}
    
#### Edit a meeting room
**Route**

    POST /api/booking/edit
**Body**

    {
      "booking_id": 1,
      "started_at" : "2017-08-02 15:00",
      "ended_at" : "2017-08-02 16:30",
    }
**Response**

    {"status":"success","message":"This booking is edited succefully."}
    or
    {"status":"error","message":"Booking time is overlapped"}
    
#### Get a list of all meetings filtered by a date range
**Route**

    POST /api/booking/list
**Body**

    {
      "started_at" : "2017-08-01 01:00",
      "ended_at" : "2017-08-02 17:00",
    }
**Response**

    [
        {
            "id": 2,
            "user_id": 11,
            "room_id": 2,
            "started_at": "2017-08-02 01:00:00",
            "ended_at": "2017-08-02 01:30:00",
            "canceled": 0,
            "created_at": "2017-08-01 05:57:30",
            "updated_at": "2017-08-01 05:57:30"
        }
    ]
    
#### Get a list of all meetings for a given user, filtered by a date range
**Route**

    POST /api/user/bookings
**Body**

    {
      "user_id" : 1, // Optional, if none, current user's meetings
      "started_at" : "2017-08-01 01:00",
      "ended_at" : "2017-08-02 17:00",
    }
**Response**

    [
        {
            "id": 2,
            "user_id": 11,
            "room_id": 2,
            "started_at": "2017-08-02 01:00:00",
            "ended_at": "2017-08-02 01:30:00",
            "canceled": 0,
            "created_at": "2017-08-01 05:57:30",
            "updated_at": "2017-08-01 05:57:30"
        }
    ]
    
#### Get a list of all meetings in a given room, filtered by a date range
**Route**

    POST /api/room/list
**Body**

    {
      "room_id" : 1, 
      "started_at" : "2017-08-01 01:00",
      "ended_at" : "2017-08-02 17:00",
    }
**Response**

    [
        {
            "id": 2,
            "user_id": 11,
            "room_id": 2,
            "started_at": "2017-08-02 01:00:00",
            "ended_at": "2017-08-02 01:30:00",
            "canceled": 0,
            "created_at": "2017-08-01 05:57:30",
            "updated_at": "2017-08-01 05:57:30"
        }
    ]
    
#### Invite other users to the meeting
**Route**

    POST /api/invitation/invite
**Body**

    {
      "invitee_id" : 2,
      "booking_id" : 1,
    }
**Response**

    {"status": "success", "message": "The invitation sent succefully."}

#### Get a list of all invitations sent
**Route**

    GET /api/invitation/sent
**Response**

    [
        {
            "id": 1,
            "inviter_id": 11,
            "invitee_id": 4,
            "booking_id": 2,
            "status": 0,
            "created_at": "2017-08-01 06:03:56",
            "updated_at": "2017-08-01 06:03:56"
        }
    ]
#### Get a list of all invitations received
**Route**

    GET /api/invitation/received
**Response**

    [
        {
            "id": 1,
            "inviter_id": 11,
            "invitee_id": 4,
            "booking_id": 2,
            "status": 0,
            "created_at": "2017-08-01 06:03:56",
            "updated_at": "2017-08-01 06:03:56"
        }
    ]
#### Accept or Reject invitation
**Route**

    POST /api/invitation/action
**Body**

    {
    	"invitation_id": 1,
    	"action": 1 // 1 => accept, 2 => reject
    }
**Response**

    {"status": "success", "message": "The invitation updated succefully."}