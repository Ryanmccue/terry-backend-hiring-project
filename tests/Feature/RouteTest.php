<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RouteTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
    	$error_auth = [
            'status' => 'error',
            'message' => 'You must be logged in'
        ];

        $error_guest = [
            'status' => 'error',
            'message' => 'You must be logged out first'
        ];

        $response = $this->get('/api/all-users');
        $response->assertStatus(200);

        $response = $this->get('/api/all-rooms');
        $response->assertStatus(200);

        $response = $this->get('/api/all-bookings');
        $response->assertStatus(200);

        $response = $this->get('/api/all-invitations');
        $response->assertStatus(200);

        $response = $this->get('/api/invitation/sent');
        $response->assertExactJson($error_auth);

        $response = $this->get('/api/invitation/received');
        $response->assertExactJson($error_auth);


        $response = $this->json('Post', '/api/user/bookings');
        $response->assertExactJson($error_auth);

        $response = $this->json('Post', '/api/room/bookings');
        $response->assertExactJson($error_auth);

        $response = $this->json('Post', '/api/booking/book');
        $response->assertExactJson($error_auth);

        $response = $this->json('Post', '/api/booking/cancel');
        $response->assertExactJson($error_auth);

        $response = $this->json('Post', '/api/booking/edit');
        $response->assertExactJson($error_auth);

        $response = $this->json('Post', '/api/booking/list');
        $response->assertExactJson($error_auth);

        $response = $this->json('Post', '/api/invitation/invite');
        $response->assertExactJson($error_auth);

        $response = $this->json('Post', '/api/invitation/action');
        $response->assertExactJson($error_auth);

        $response = $this->get('/api/logout');
        $response->assertExactJson($error_auth);

        $response = $this->json('Delete', '/api/unregister');
        $response->assertExactJson($error_auth);


        $response = $this->json('Post', '/api/register',  
        	['name' => 'test', 'email' => 'test@gmail.com', 
        	'password' => 'testpwd', 'password_confirmation' =>'testpwd']);
        $response->assertJson([
            'status' => 'success'
        ]);

        $response = $this->json('Post', '/api/login',
        	['email' => 'test@gmail.com', 
        	'password' => 'testpwd']);
        $response->assertJson([
            'status' => 'success'
        ]);

        $response = $this->json('Post', '/api/register',  
        	['name' => 'test', 'email' => 'test@gmail.com', 
        	'password' => 'testpwd', 'password_confirmation' =>'testpwd']);
        $response->assertExactJson($error_guest);

        $response = $this->json('Post', '/api/login',
        	['email' => 'test@gmail.com', 
        	'password' => 'testpwd']);
        $response->assertExactJson($error_guest);

        $response = $this->json('Delete', '/api/unregister');
        $response->assertJson([
            'status' => 'success'
        ]);
    }
}
