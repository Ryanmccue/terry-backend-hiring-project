<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    //

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

   /**
     * Get the bookings for the user
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    /**
     * Get all invitations that the user sent.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function invitationSent(){
        return $this->hasMany(Invitation::class, 'inviter_id');
    }

    /**
     * Get all invitations that the user received.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function invitationReceived(){
        return $this->hasMany(Invitation::class, 'invitee_id');
    }
}
