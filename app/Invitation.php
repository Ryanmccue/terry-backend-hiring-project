<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'inviter_id', 'invitee_id', 'booking_id', 'status'
 	];


    /**
     * Scope a query to only include a booking of a given booking id
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $booking_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
 	public function scopeWhereBooking($query, $booking_id){
 		$query->where('booking_id', $booking_id);
 	}

    /**
     * Scope a query to only include received invitations of a given user and in status of wait or accepted.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $invitee_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
 	public function scopeInvited($query, $invitee_id){
 		$query->where('invitee_id', $invitee_id)->where('status', '<', '2');
 	}
 }
