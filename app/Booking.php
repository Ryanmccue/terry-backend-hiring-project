<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'room_id', 'started_at', 'ended_at', 'canceled'
 	];

    /**
     * Get the room of the booking
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
 	public function rooms()
    {
        return $this->belongsTo(Room::class);
    }

    /**
     * Get the creator of the booking
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function users()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all of the invited memebers for the booking where status is accepted.
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function members(){
        return $this->hasMany(Invitation::class)->where('status', 1);
    }

    /**
     * Scope a query to only include bookings of a given canceld flag
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $flag
     * @return \Illuminate\Database\Eloquent\Builder
     */
 	public function scopeCanceled($query, $flag){
 		return $query->where('canceled', $flag);
 	}

    /**
     * Scope a query to only include bookings of a given date range
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $started_at
     * @param mixed $ended_at
     * @return \Illuminate\Database\Eloquent\Builder
     */
 	public function scopeDateRange($query, $started_at, $ended_at){
 		return $query
 		->where('started_at', '<', $ended_at)
        ->where('ended_at', '>', $started_at);
 	}

    /**
     * Scope a query to only include bookings of a given room id
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $room_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
 	public function scopeWhereRoom($query, $room_id){
 		return $query->where('room_id', '=', $room_id);
 	}

    /**
     * Scope a query to only include bookings of a given user id
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $user_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
 	public function scopeWhereUser($query, $user_id){
 		return $query->where('user_id','=', $user_id);
 	}

    /**
     * Scope a query to only include bookings of a given room id and date range
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $room_id
     * @param mixed $started_at
     * @param mixed $ended_at
     * @return \Illuminate\Database\Eloquent\Builder
     */
 	public function scopeScheduled($query, $room_id, $started_at, $ended_at){
 		return $query
 		->whereRoom($room_id)
        ->canceled(false)
        ->dateRange($started_at, $ended_at);
 	}
}
