<?php

namespace App\Http\Controllers;

use App\Room;
use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BookingController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('myauth', ['except' => ['index']]);
    }

    /**
     * Return all bookings in DB
     *
     * @return array App\Booking
     */
    public function index(){
        return Booking::all();
    }

    /**
     * Book a meeting room for given date and time
     *
     * @return Response success or error with a message
     */
    public function book()
    {
        //Get all request data
        $data = request()->all();

        //Create a validator
        $validator = Validator::make($data, [
            'room_number' => 'required|integer|exists:rooms,room_number',
            'started_at' => 'required|date_format:Y-m-d H:i|before:ended_at',
            'ended_at' => 'required|date_format:Y-m-d H:i|after:started_at',
        ]);

        //Return errors if failed to validate inputs
        if($validator->fails())
                return response()->error($validator->messages());

        //Get the room by a given room number
        $room = Room::whereRoomNumber($data['room_number'])->first();

         //If the booking of the given room exsits in the given date range
        if(Booking::scheduled($room->id, $data['started_at'], $data['ended_at'])->count())
            return response()->error("Booking time is overlapped");
        
        //Create new booking
        Booking::create([
            'user_id' => Auth::user()->id,
            'room_id' => $room->id,
            'started_at' => $data['started_at'],
            'ended_at' => $data['ended_at']
        ]);

        return response()->success("This booking created succefully.");
    }

    /**
     * Cancel a selected booking to a given time range
     *
     * @return Response success or error with a message
     */
    public function cancel()
    {
        //Get all request data
        $data = request()->all();

        //Create a validator
        $validator = Validator::make($data, [
            'booking_id' => 'required|integer|exists:bookings,id'
        ]);

        //Return errors if failed to validate inputs
        if($validator->fails())
                return response()->error($validator->messages());

        //Get the booking of the given booking id
        $booking = Booking::find($data['booking_id']);

        //If not the creator of the booking
        if($booking->user_id != Auth::user()->id)
            return response()->error("You cannot cancel this booking!");

        //If booking is canceld
        if($booking->canceled)
            return response()->error("This Booking is already canceled!");

        //Update the booking to be canceled
        $booking->update(['canceled' => true]);

        return response()->success("This booking is canceld succefully.");
    }

    /**
     * Edit the time of a selected booking to a given time range
     *
     * @return Response success or error with a message
     */
    public function edit()
    {
        //Get all request data
        $data = request()->all();

        //Create a validator
        $validator = Validator::make($data, [
            'booking_id' => 'required|integer|exists:bookings,id',
            'started_at' => 'required|date_format:Y-m-d H:i|before:ended_at',
            'ended_at' => 'required|date_format:Y-m-d H:i|after:started_at',
        ]);

        //Return errors if failed to validate inputs
        if($validator->fails())
                return response()->error($validator->messages());

        //Get the booking of the given booking id
        $booking = Booking::find($data['booking_id']);

        //If not the creator of the booking
        if($booking->user_id != Auth::user()->id)
            return response()->error("You cannot edit this booking!");

        //If booking is canceld
        if($booking->canceled)
            return response()->error("This Booking is already canceled!");

        //If the booking of the given room exsits in the given date range
        if(Booking::scheduled($booking->room_id, $data['started_at'], $data['ended_at'])->count())
            return response()->error("Booking time is overlapped");

        //Update the date range to the given range
        $booking->update(['started_at' => $data['started_at'], 'ended_at' => $data['ended_at']]);

        return response()->success("This booking is edited succefully.");    
    }


    /**
     *  Return a list of all meetings, filtered by a date range 
     *
     * @return array App\Booking
     */
    public function listBookings()
    {
        //Get all request data
        $data = request()->all();

        //Create a validator
        $validator = Validator::make($data, [
            'started_at' => 'required|date_format:Y-m-d H:i|before:ended_at',
            'ended_at' => 'required|date_format:Y-m-d H:i|after:started_at',
        ]);

        //Return errors if failed to validate inputs
        if($validator->fails())
            return response()->error($validator->messages());

        //Retrun all active bookings of given date range
        return Booking::dateRange($data['started_at'], $data['ended_at'])->canceled(false)->get();
    }
}
