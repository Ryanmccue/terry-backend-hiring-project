<?php

namespace App\Http\Controllers;

use App\User;
use App\Booking;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('myauth', ['except' => ['index']]);
    }

    /**
     * return all users in DB
     *
     * @return array App\User
     */
    public function index()
    {
        return User::all();
    }


    /**
     *  return a list of all meetings for a given user, filtered by a date range 
     *
     * @return array App\Booking
     */
    public function listBookings()
    {
        //Get all request data
        $data = request()->all();

        //Create a validator
        $validator = Validator::make($data, [
        	'user_id' => 'integer|exists:users,id',
            'started_at' => 'required|date_format:Y-m-d H:i|before:ended_at',
            'ended_at' => 'required|date_format:Y-m-d H:i|after:started_at',
        ]);

        //Return errors if failed to validate inputs
        if($validator->fails())
            return response()->error($validator->messages());

        //if user_id was in request data, get all booking of given user,
        //otherwise, get all booking of authenticated user
        $user_id = (request()->has('user_id')) ? $data['user_id'] : Auth::user()->id;

        return Booking::dateRange($data['started_at'], $data['ended_at'])->canceled(false)->whereUser($user_id)->get();
    }

}