<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Room;
use App\Invitation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class InviteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('myauth', ['except' => ['index']]);
    }

    /**
     * return all invitations in DB
     *
     * @return array App\Invitations
     */
    public function index(){
        return Invitation::all();
    }

    /**
     * return array of invitations sent
     *
     * @return array App\Invitations
     */
    public function listSent(){
        return Auth::user()->invitationSent()->get();
    }

    /**
     * return array of invitations received
     *
     * @return array App\Invitations
     */
    public function listReceived(){
        return Auth::user()->invitationReceived()->get();
    }

    /**
     * invite a user to a booked meeting room.
     *
     * @return Response success or error with a message
     */
    public function invite(){
        //Get all request data
    	$data = request()->all();

        //Create a validator
        $validator = Validator::make($data, [
            'invitee_id' => 'required|integer|exists:users,id',
            'booking_id' => 'required|integer|exists:bookings,id'
        ]);

        //Return errors if failed to validate inputs
        if($validator->fails())
                return response()->error($validator->messages());

        //If The user tries to invtie itself
        if($data['invitee_id'] == Auth::user()->id)
            return response()->error("You cannot invite yourself!");

        //Get the booking by given booking id
        $booking = Booking::find($data['booking_id']);
        //Get the room by given room id
        $room = Room::find($booking->room_id);

        //If not the creator of the booking
        if($booking->user_id != Auth::user()->id)
            return response()->error("You must be the creator of the booking to invite a member!");

        //If booking is canceld
        if($booking->canceled)
            return response()->error("This Booking is already canceled!");

        //If the user already invited
        if(Invitation::whereBooking($booking->id)->invited($data['invitee_id'])->count())
            return response()->error("This memeber is already invited to this booking!");

        //if the number of invited user exeeds the capcity of the room
        if($booking->members->count() >= $room->capacity - 1)
            return response()->error("The capacity of this room is full!");

        //Create a new invitation
        Invitation::create([
            'inviter_id' => Auth::user()->id,
            'invitee_id' => $data['invitee_id'],
            'booking_id' => $booking->id,
            'status' => 0
        ]);

        return response()->success("The invitation sent succefully.");
    }

    /**
     * accept or reject a received invitation.
     *
     * @return Response success or error with a message
     */
    public function action(){
        //Get all request data
    	$data = request()->all();

        //Create a validator
        $validator = Validator::make($data, [
            'invitation_id' => 'required|integer|exists:invitations,id',
            'action' => 'required|integer|between:1,2'
        ]);

        //Return errors if failed to validate inputs
        if($validator->fails())
                return response()->error($validator->messages());

        //Get the invitation by given invitation id
        $invitation = invitation::find($data['invitation_id']);
        //Get the booking by given booking id
        $booking = Booking::find($invitation->booking_id);
        //Get the room by given room id
        $room = Room::find($booking->room_id);

        //if the invitation was not sent to the user
    	if($invitation->invitee_id != Auth::user()->id)
            return response()->error("This invitation was not sent to you!");

        //if the invitation is not in wait status
        if($invitation->status != 0)
            return response()->error("This invitation was already processed.");

        //if the number of invited user exeeds the capcity of the room
        if($data['action'] == 1 && $booking->members->count() >= $room->capacity - 1)
            return response()->error("The capacity of this room is full!");

        //Update the invitation to have the given status
        $invitation->update(['status' => $data['action']]);

        return response()->success("The invitation updated succefully.");
    }
}
