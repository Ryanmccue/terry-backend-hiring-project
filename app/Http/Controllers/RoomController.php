<?php

namespace App\Http\Controllers;

use App\Room;
use App\Booking;
use Illuminate\Http\Request;

class RoomController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('myauth', ['except' => ['index']]);
    }

    /**
     * return all rooms in DB
     *
     * @return array App\Room
     */
    public function index()
    {
        //
        return Room::all();
    }

    /**
     *  return a list of all meetings in a given room, filtered by a date range 
     *
     * @return array App\Booking
     */
    public function listBookings()
    {
        //Get all request data
        $data = request()->all();

        //Create a validator
        $validator = Validator::make($data, [
            'room_id' => 'required|integer|exists:rooms,id',
            'started_at' => 'required|date_format:Y-m-d H:i|before:ended_at',
            'ended_at' => 'required|date_format:Y-m-d H:i|after:started_at',
        ]);

        //Return errors if failed to validate inputs
        if($validator->fails())
            return response()->error($validator->messages());

        return Booking::dateRange($data['started_at'], $data['ended_at'])->canceled(false)->whereRoom($data['room_id'])->get();
    }

}
