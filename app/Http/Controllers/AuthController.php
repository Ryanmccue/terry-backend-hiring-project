<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('myauth', ['only' => ['logout', 'unregister']]);
        $this->middleware('myguest', ['only' => ['register', 'login']]);
    }

    /**
     * Register a new user
     *
     * @return Response success or error with a message
     */
    public function register()
    {
        //Get all request data
        $data = request()->all();
        
        //Create a validator
        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        //Return errors if failed to validate inputs
        if($validator->fails())
            return response()->error($validator->messages());

        //Create a new user
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        //Return success message
        return response()->success('Registered succefully');
    }


    /**
     * Register a new user
     *
     * @return Response success or error with a message
     */
    public function unregister()
    {
        Auth::user()->delete();

        //Return success message
        return response()->success('Unregistered succefully');
    }

    /**
     * Login with email and password
     *
     * @return Response success or error with a message
     */
    public function login()
    {
        //Get all request data
    	$data = request()->all();

        //Create a validator
        $validator = Validator::make($data, [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6'
        ]);    

        //Return errors if failed to validate inputs
        if($validator->fails())
            return $validator->messages();

        //Return success message if succeed in login
        if(Auth::attempt(['email' => $data['email'],'password' => $data['password']]))
        	return response()->success('Logged in succefully');

        //If login failed
        return response()->error('Logged in failed');
    }

    /**
     * Logout
     *
     * @return Response success or error with a message
     */
    public function logout()
    {
        Auth::logout();
        return response()->success('Logged out succefully');
    }
}
