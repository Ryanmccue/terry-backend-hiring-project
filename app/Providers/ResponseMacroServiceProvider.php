<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;


class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //return status success with a given message
        Response::macro('success', function ($value) {
            return response()->json([
                'status' => 'success',
                'message' => $value
            ]);
        });   

        //return status error with a given message
        Response::macro('error', function ($value) {
            return response()->json([
                'status' => 'error',
                'message' => $value
            ]);
        });     
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
