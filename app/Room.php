<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    //
    //

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'room_number', 'capacity'
    ];


   /**
     * Get the bookings of the room
     * 
     * @return \Illuminate\Database\Eloquent\Builder
     */
 	public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    /**
     * Scope a query to only include rooms of a given room number
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $room_number
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereRoomNumber($query, $room_number){
    	return $query->where('room_number', $room_number);
    }
}
