<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
});

//returning all users in DB
Route::get('/all-users', "UserController@index");
//returning all rooms in DB
Route::get('/all-rooms', "RoomController@index");
//returning all bookings in DB
Route::get('/all-bookings', "BookingController@index");
//returning all invitations in DB
Route::get('/all-invitations', "InviteController@index");


//Authentication
Route::post('/register', "AuthController@register");
Route::delete('/unregister', "AuthController@unregister");
Route::post('/login', "AuthController@login");
Route::get('/logout', "AuthController@logout");

//User
Route::post('/user/bookings', "UserController@listBookings");

//Room
Route::post('/room/bookings', "RoomController@listBookings");

//Booking
Route::post('/booking/book', "BookingController@book");
Route::post('/booking/cancel', "BookingController@cancel");
Route::post('/booking/edit', "BookingController@edit");
Route::post('/booking/list', "BookingController@listBookings");

//Invitations
Route::post('/invitation/invite', "InviteController@invite");
Route::post('/invitation/action', "InviteController@action");
Route::get('/invitation/sent', "InviteController@listSent");
Route::get('/invitation/received', "InviteController@listReceived");
