<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inviter_id')->unsigned();
            $table->integer('invitee_id')->unsigned();
            $table->integer('booking_id')->unsigned();
            $table->foreign('inviter_id')->references('id')->on('users');
            $table->foreign('invitee_id')->references('id')->on('users');
            $table->foreign('booking_id')->references('id')->on('bookings');
            $table->integer('status')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitations');
    }
}
