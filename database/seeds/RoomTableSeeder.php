<?php

use Illuminate\Database\Seeder;
use App\Room;

class RoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	Room::create([
            'room_number' => 101,
            'capacity' => 1
    	]);

        Room::create([
            'room_number' => 102,
            'capacity' => 2
        ]);

        Room::create([
            'room_number' => 103,
            'capacity' => 12
        ]);
    }
}
